package com.threaddoctor.jit;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.threaddoctor.jit.bean.JITDataOverview;
import com.threaddoctor.jit.service.api.JITDataService;

@Controller
@RequestMapping("/jit")
public class HotSpotLogController {
	private static final String JIT_OVERVIEW = "jitoverview";
	@Autowired
	JITDataService service;

	// Logger loger = LoggerFactory.(ThreadDataController.class);

	@GetMapping("/uploadform")
	public String logUploadForm(Model model) {
		return "jit/loguploadform";
	}

	@PostMapping("/upload")
	public String uploadFile(MultipartFile file, HttpSession session,
			Model model) {
		JITDataOverview overview = service.getJITDataOverview(file);
		// loger.debug("File name: " + file.getName());
		session.setAttribute(JIT_OVERVIEW, overview);
		return "redirect:/jit/jitreport";
	}

	@GetMapping("/jitreport")
	public String getJITReport(Model model, HttpSession session) {
		JITDataOverview overview = (JITDataOverview) session
				.getAttribute(JIT_OVERVIEW);
		if (overview != null) {
			// loger.debug("Threadoverview Summary: " + overview);
			model.addAttribute(JIT_OVERVIEW, session.getAttribute(JIT_OVERVIEW));
		} else {
			// loger.debug("Threadreport data not present in session, redirecting to uploadform file.");
			return "redirect:/threadreport";
		}
		return "jit/jitlogdata";
	}
}
