package com.threaddoctor.jit.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.threaddoctor.jit.bean.JITDataOverview;
import com.threaddoctor.jit.service.api.JITDataService;
import com.threaddoctor.jit.util.LogParserUtil;

@Service
public class JITDataServiceImpl implements JITDataService {

	@Override
	public JITDataOverview getJITDataOverview(MultipartFile file) {
		return LogParserUtil.parse(file);
	}
}
