package com.threaddoctor.jit.service.api;

import org.springframework.web.multipart.MultipartFile;

import com.threaddoctor.jit.bean.JITDataOverview;

public interface JITDataService {

	JITDataOverview getJITDataOverview(MultipartFile file);
}
