package com.threaddoctor.jit.bean;

public class NativeMethod {

	private String compileId;
	private String compiler;
	private String compileKind;
	private String entry;
	private String address;
	private String method;
	private Long level;
	private Long size;
	private Long relocationOffset;
	private Long instsOffset;
	private Long stubOffset;
	private Long scopesDataOffset;
	private Long scopesPcsOffset;
	private Long dependenciesOffset;
	private Long nulChkTableOffset;
	private Long oopsOffset;
	private Long countsOffset;
	private Long bytes;
	private Long count;
	private Long iicount;
	private Float stamp;

	public String getCompileId() {
		return compileId;
	}

	public void setCompileId(String compileId) {
		this.compileId = compileId;
	}

	public String getCompiler() {
		return compiler;
	}

	public void setCompiler(String compiler) {
		this.compiler = compiler;
	}

	public String getEntry() {
		return entry;
	}

	public void setEntry(String entry) {
		this.entry = entry;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Long getRelocationOffset() {
		return relocationOffset;
	}

	public void setRelocationOffset(Long relocationOffset) {
		this.relocationOffset = relocationOffset;
	}

	public Long getInstsOffset() {
		return instsOffset;
	}

	public void setInstsOffset(Long instsOffset) {
		this.instsOffset = instsOffset;
	}

	public Long getStubOffset() {
		return stubOffset;
	}

	public void setStubOffset(Long stubOffset) {
		this.stubOffset = stubOffset;
	}

	public Long getScopesDataOffset() {
		return scopesDataOffset;
	}

	public void setScopesDataOffset(Long scopesDataOffset) {
		this.scopesDataOffset = scopesDataOffset;
	}

	public Long getScopesPcsOffset() {
		return scopesPcsOffset;
	}

	public void setScopesPcsOffset(Long scopesPcsOffset) {
		this.scopesPcsOffset = scopesPcsOffset;
	}

	public Long getDependenciesOffset() {
		return dependenciesOffset;
	}

	public void setDependenciesOffset(Long dependenciesOffset) {
		this.dependenciesOffset = dependenciesOffset;
	}

	public Long getNulChkTableOffset() {
		return nulChkTableOffset;
	}

	public void setNulChkTableOffset(Long nulChkTableOffset) {
		this.nulChkTableOffset = nulChkTableOffset;
	}

	public Long getBytes() {
		return bytes;
	}

	public void setBytes(Long bytes) {
		this.bytes = bytes;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getIicount() {
		return iicount;
	}

	public void setIicount(Long iicount) {
		this.iicount = iicount;
	}

	public Float getStamp() {
		return stamp;
	}

	public void setStamp(Float stamp) {
		this.stamp = stamp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NativeMethod [compileId=");
		builder.append(compileId);
		builder.append(", compiler=");
		builder.append(compiler);
		builder.append(", compileKind=");
		builder.append(compileKind);
		builder.append(", entry=");
		builder.append(entry);
		builder.append(", address=");
		builder.append(address);
		builder.append(", method=");
		builder.append(method);
		builder.append(", level=");
		builder.append(level);
		builder.append(", size=");
		builder.append(size);
		builder.append(", relocationOffset=");
		builder.append(relocationOffset);
		builder.append(", instsOffset=");
		builder.append(instsOffset);
		builder.append(", stubOffset=");
		builder.append(stubOffset);
		builder.append(", scopesDataOffset=");
		builder.append(scopesDataOffset);
		builder.append(", scopesPcsOffset=");
		builder.append(scopesPcsOffset);
		builder.append(", dependenciesOffset=");
		builder.append(dependenciesOffset);
		builder.append(", nulChkTableOffset=");
		builder.append(nulChkTableOffset);
		builder.append(", oopsOffset=");
		builder.append(oopsOffset);
		builder.append(", countsOffset=");
		builder.append(countsOffset);
		builder.append(", bytes=");
		builder.append(bytes);
		builder.append(", count=");
		builder.append(count);
		builder.append(", iicount=");
		builder.append(iicount);
		builder.append(", stamp=");
		builder.append(stamp);
		builder.append("]");
		return builder.toString();
	}

	public Long getOopsOffset() {
		return oopsOffset;
	}

	public void setOopsOffset(Long oopsOffset) {
		this.oopsOffset = oopsOffset;
	}

	public String getCompileKind() {
		return compileKind;
	}

	public void setCompileKind(String compileKind) {
		this.compileKind = compileKind;
	}

	public Long getCountsOffset() {
		return countsOffset;
	}

	public void setCountsOffset(Long countsOffset) {
		this.countsOffset = countsOffset;
	}

}
