package com.threaddoctor.jit.bean;

import java.util.ArrayList;
import java.util.List;

public class Phase {
	private String name;
	private float startStamp;
	private float endStamp;
	private List<Phase> phases;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Phase> getPhases() {
		if (phases == null) {
			phases = new ArrayList<>();
		}
		return phases;
	}

	public void setPhases(List<Phase> phases) {
		this.phases = phases;
	}

	public float getStartStamp() {
		return startStamp;
	}

	public void setStartStamp(float startStamp) {
		this.startStamp = startStamp;
	}

	public float getEndStamp() {
		return endStamp;
	}

	public void setEndStamp(float endStamp) {
		this.endStamp = endStamp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Phase [name=");
		builder.append(name);
		builder.append(", startStamp=");
		builder.append(startStamp);
		builder.append(", endStamp=");
		builder.append(endStamp);
		builder.append(", phases=");
		builder.append(phases);
		builder.append("]");
		return builder.toString();
	}

}
