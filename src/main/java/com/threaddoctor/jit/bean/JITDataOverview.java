package com.threaddoctor.jit.bean;

import java.util.List;
import java.util.stream.Collectors;

public class JITDataOverview {
	private String fileName;
	private String jvmVersion;
	private List<CodeCache> codeCaches;
	private List<Method> methods;
	private List<NativeMethod> nativeMethods;
	private List<Task> tasks;

	public List<CodeCache> getCodeCaches() {
		return codeCaches;
	}

	public void setCodeCaches(List<CodeCache> codeCaches) {
		this.codeCaches = codeCaches;
	}

	public List<Method> getMethods() {
		return methods;
	}

	public void setMethods(List<Method> methods) {
		this.methods = methods;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getJvmVersion() {
		return jvmVersion;
	}

	public void setJvmVersion(String jvmVersion) {
		this.jvmVersion = jvmVersion;
	}

	public List<Long> getCodeCacheFreeSize() {
		return getCodeCaches().stream().map(p -> p.getFreeCodeCache())
				.collect(Collectors.toList());
	}

	public List<Float> getCodeCacheTimings() {
		return getCodeCaches().stream().map(p -> p.getStamp())
				.collect(Collectors.toList());
	}

	public List<NativeMethod> getNativeMethods() {
		return nativeMethods;
	}

	public void setNativeMethods(List<NativeMethod> nativeMethods) {
		this.nativeMethods = nativeMethods;
	}

}
