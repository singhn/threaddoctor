package com.threaddoctor.jit.bean;

public class Method {
	private String id;
	private String holder;
	private String returnBytes;
	private String arguments;
	private String flags;
	private Long bytes;
	private Long iicount;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHolder() {
		return holder;
	}

	public void setHolder(String holder) {
		this.holder = holder;
	}

	public String getReturnBytes() {
		return returnBytes;
	}

	public void setReturnBytes(String returnBytes) {
		this.returnBytes = returnBytes;
	}

	public String getArguments() {
		return arguments;
	}

	public void setArguments(String arguments) {
		this.arguments = arguments;
	}

	public String getFlags() {
		return flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public Long getBytes() {
		return bytes;
	}

	public void setBytes(Long bytes) {
		this.bytes = bytes;
	}

	public Long getIicount() {
		return iicount;
	}

	public void setIicount(Long iicount) {
		this.iicount = iicount;
	}

}
