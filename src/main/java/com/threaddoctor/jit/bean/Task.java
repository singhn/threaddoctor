package com.threaddoctor.jit.bean;

import java.util.ArrayList;
import java.util.List;

public class Task {
	private String compileId;
	private String compileKind;
	private String method;
	private Long bytes;
	private Long inlinedBytes;
	private Long nativeMethodSize;
	private Long count;
	private Long iicount;
	private Long backedgeCount;
	private Long level;
	private float stamp;
	private float doneStamp;
	private boolean blocking;
	private boolean success;
	private Long osrBci;
	private List<Phase> phases;

	public String getCompileId() {
		return compileId;
	}

	public void setCompileId(String compileId) {
		this.compileId = compileId;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Long getBytes() {
		return bytes;
	}

	public void setBytes(Long bytes) {
		this.bytes = bytes;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getIicount() {
		return iicount;
	}

	public void setIicount(Long iicount) {
		this.iicount = iicount;
	}

	public Long getLevel() {
		return level;
	}

	public void setLevel(Long level) {
		this.level = level;
	}

	public float getStamp() {
		return stamp;
	}

	public void setStamp(float stamp) {
		this.stamp = stamp;
	}

	public List<Phase> getPhases() {
		if (phases == null) {
			phases = new ArrayList<>();
		}
		return phases;
	}

	public void setPhases(List<Phase> phases) {
		this.phases = phases;
	}

	public float getDoneStamp() {
		return doneStamp;
	}

	public void setDoneStamp(float doneStamp) {
		this.doneStamp = doneStamp;
	}

	public String getCompileKind() {
		return compileKind;
	}

	public void setCompileKind(String compileKind) {
		this.compileKind = compileKind;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Task [compileId=");
		builder.append(compileId);
		builder.append(", compileKind=");
		builder.append(compileKind);
		builder.append(", method=");
		builder.append(method);
		builder.append(", bytes=");
		builder.append(bytes);
		builder.append(", inlinedBytes=");
		builder.append(inlinedBytes);
		builder.append(", nativeMethodSize=");
		builder.append(nativeMethodSize);
		builder.append(", count=");
		builder.append(count);
		builder.append(", iicount=");
		builder.append(iicount);
		builder.append(", backedgeCount=");
		builder.append(backedgeCount);
		builder.append(", level=");
		builder.append(level);
		builder.append(", stamp=");
		builder.append(stamp);
		builder.append(", doneStamp=");
		builder.append(doneStamp);
		builder.append(", blocking=");
		builder.append(blocking);
		builder.append(", success=");
		builder.append(success);
		builder.append(", osrBci=");
		builder.append(osrBci);
		builder.append(", phases=");
		builder.append(phases);
		builder.append("]");
		return builder.toString();
	}

	public boolean isBlocking() {
		return blocking;
	}

	public void setBlocking(boolean blocking) {
		this.blocking = blocking;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Long getInlinedBytes() {
		return inlinedBytes;
	}

	public void setInlinedBytes(Long inlinedBytes) {
		this.inlinedBytes = inlinedBytes;
	}

	public Long getNativeMethodSize() {
		return nativeMethodSize;
	}

	public void setNativeMethodSize(Long nativeMethodSize) {
		this.nativeMethodSize = nativeMethodSize;
	}

	public Long getBackedgeCount() {
		return backedgeCount;
	}

	public void setBackedgeCount(Long backedgeCount) {
		this.backedgeCount = backedgeCount;
	}

	public Long getOsrBci() {
		return osrBci;
	}

	public void setOsrBci(Long osrBci) {
		this.osrBci = osrBci;
	}

}
