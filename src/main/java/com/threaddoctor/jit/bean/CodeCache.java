package com.threaddoctor.jit.bean;

public class CodeCache {
	private Long totalBlobs;
	private Long nMethods;
	private Long adapters;
	private Long freeCodeCache;
	private Float stamp;
	private Task task;

	public Long getTotalBlobs() {
		return totalBlobs;
	}

	public void setTotalBlobs(Long totalBlobs) {
		this.totalBlobs = totalBlobs;
	}

	public Long getnMethods() {
		return nMethods;
	}

	public void setnMethods(Long nMethods) {
		this.nMethods = nMethods;
	}

	public Long getAdapters() {
		return adapters;
	}

	public void setAdapters(Long adapters) {
		this.adapters = adapters;
	}

	public Long getFreeCodeCache() {
		return freeCodeCache;
	}

	public void setFreeCodeCache(Long freeCodeCache) {
		this.freeCodeCache = freeCodeCache;
	}

	public Float getStamp() {
		if (stamp == null && task != null) {
			this.stamp = task.getDoneStamp();
		}
		return this.stamp;
	}

	public void setStamp(Float stamp) {
		this.stamp = stamp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CodeCache [totalBlobs=");
		builder.append(totalBlobs);
		builder.append(", nMethods=");
		builder.append(nMethods);
		builder.append(", adapters=");
		builder.append(adapters);
		builder.append(", freeCodeCache=");
		builder.append(freeCodeCache);
		builder.append(", stamp=");
		builder.append(stamp);
		builder.append("]");
		return builder.toString();
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}
}
