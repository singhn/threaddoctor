package com.threaddoctor.jit.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.springframework.web.multipart.MultipartFile;

import com.threaddoctor.jit.bean.CodeCache;
import com.threaddoctor.jit.bean.JITDataOverview;
import com.threaddoctor.jit.bean.NativeMethod;
import com.threaddoctor.jit.bean.Phase;
import com.threaddoctor.jit.bean.Task;

public class LogParserUtil {

	private static final String NMETHOD = "nmethod";
	private static final String STAMP = "stamp";
	private static final String TASK_DONE = "task_done";
	private static final String METHOD = "method";
	private static final String TASK = "task";
	private static final String PHASE = "phase";
	private static final String PROPERTIES = "properties";
	private static final String LAUNCHER = "launcher";

	public static JITDataOverview parse(MultipartFile file) {
		JITDataOverview overview = new JITDataOverview();
		overview.setFileName(file.getOriginalFilename());
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

		Map<String, Long> methodMap = new HashMap<>();
		try {
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(file
					.getInputStream());
			// for VM version
			while (reader.hasNext()) {
				XMLEvent nextEvent = reader.nextEvent();
				if (nextEvent.isStartElement()) {
					StartElement startElement = nextEvent.asStartElement();
					if ("tty".equals(startElement.getName().getLocalPart())) {
						break;
					}
					nextEvent = reader.nextEvent();
					switch (startElement.getName().getLocalPart()) {
					case "name":
						overview.setJvmVersion(nextEvent.asCharacters()
								.getData());
						System.out.println("name:"
								+ nextEvent.asCharacters().getData());
						break;
					case "release":
						System.out.println("release:"
								+ nextEvent.asCharacters().getData());
						break;
					case "info":
						System.out.println("info:"
								+ nextEvent.asCharacters().getData());
						break;
					case "args":
						System.out.println("args:"
								+ nextEvent.asCharacters().getData());
						break;
					case "command":
						System.out.println("command: "
								+ nextEvent.asCharacters().getData());
						break;
					case LAUNCHER:
						System.out.println("launcher: "
								+ nextEvent.asCharacters().getData());
						break;
					case PROPERTIES:
						System.out.println("properties: "
								+ nextEvent.asCharacters().getData());
						break;
					default:
						break;
					}
				}
			}

			List<NativeMethod> nmethods = new ArrayList<>();
			while (reader.hasNext()) {
				XMLEvent nextEvent = reader.nextEvent();
				if (nextEvent.isStartElement()) {

					StartElement startElement = nextEvent.asStartElement();
					if ("compilation_log".equals(startElement.getName()
							.getLocalPart())) {
						break;
					}
					switch (startElement.getName().getLocalPart()) {
					case NMETHOD:
						NativeMethod method = new NativeMethod();
						nmethods.add(method);
						updateNativeMethod(getAttributeValueMap(startElement),
								method);
						break;
					}
				}
			}

			List<Task> tasks = new ArrayList<>();
			List<CodeCache> codeCaches = new ArrayList<>();
			Task task = null;
			int openPhases = 0;
			Stack<Phase> phases = null;
			while (reader.hasNext()) {
				XMLEvent nextEvent = reader.nextEvent();
				if (nextEvent.isStartElement()) {
					StartElement startElement = nextEvent.asStartElement();
					switch (startElement.getName().getLocalPart()) {
					case TASK:
						task = new Task();
						tasks.add(task);
						phases = new Stack<>();
						updateTask(getAttributeValueMap(startElement), task);
						break;
					case PHASE:
						Phase phase = new Phase();
						updatePhase(getAttributeValueMap(startElement), phase);
						if (!phases.empty()) {
							phases.get(0).getPhases().add(phase);
						} else {
							task.getPhases().add(phase);
						}
						phases.push(phase);
						break;
					case "phase_done":
						Phase phase1 = phases.pop();
						updatePhaseDone(getAttributeValueMap(startElement),
								phase1);
						break;
					case METHOD:
						Map<String, String> data = getAttributeValueMap(startElement);
						Long bytes = data.get("bytes") != null ? Long
								.valueOf(data.get("bytes")) : null;
						methodMap.putIfAbsent(data.get("name"), bytes);
						break;
					case "code_cache":
						CodeCache cCache = new CodeCache();
						cCache.setTask(task);
						codeCaches.add(cCache);
						updateCodeCache(getAttributeValueMap(startElement),
								cCache);
						break;
					case TASK_DONE:
						updateTaskDone(getAttributeValueMap(startElement), task);
						break;

					}

				}
			}
			reader.close();
			overview.setCodeCaches(codeCaches);
			overview.setTasks(tasks);
			overview.setNativeMethods(nmethods);

			for (Task task1 : tasks) {
				System.out.println(task1);
			}
			for (Map.Entry entry : methodMap.entrySet()) {
				System.out.printf("%50s | %d\n", entry.getKey(),
						entry.getValue());
			}
			for (CodeCache cCache : codeCaches) {
				System.out.printf("%10s | %d\n", cCache.getStamp(),
						cCache.getFreeCodeCache());
			}
			/*
			 * for (NativeMethod method : nmethods) {
			 * System.out.println(method); }
			 */
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return overview;
	}

	private static void updateCodeCache(Map<String, String> map,
			CodeCache cCache) {
		for (Map.Entry<String, String> entry : map.entrySet()) {
			switch (entry.getKey()) {
			case "total_blobs":
				cCache.setTotalBlobs(Long.valueOf(entry.getValue()));
				break;
			case "nmethods":
				cCache.setnMethods(Long.valueOf(entry.getValue()));
				break;
			case "adapters":
				cCache.setAdapters(Long.valueOf(entry.getValue()));
				break;
			case "free_code_cache":
				cCache.setFreeCodeCache(Long.valueOf(entry.getValue()));
				break;
			}
		}

	}

	private static void updateNativeMethod(Map<String, String> map,
			NativeMethod method) {
		for (Map.Entry<String, String> entry : map.entrySet()) {
			switch (entry.getKey()) {
			case "compile_id":
				method.setCompileId(entry.getValue());
				break;
			case "compile_kind":
				method.setCompileKind(entry.getValue());
				break;
			case "compiler":
				method.setCompiler(entry.getValue());
				break;
			case "level":
				method.setLevel(Long.valueOf(entry.getValue()));
				break;
			case "entry":
				method.setEntry(entry.getValue());
				break;
			case "size":
				method.setSize(Long.valueOf(entry.getValue()));
				break;
			case "address":
				method.setAddress(entry.getValue());
				break;
			case "relocation_offset":
				method.setRelocationOffset(Long.valueOf(entry.getValue()));
				break;
			case "insts_offset":
				method.setInstsOffset(Long.valueOf(entry.getValue()));
				break;
			case "stub_offset":
				method.setStubOffset(Long.valueOf(entry.getValue()));
				break;
			case "scopes_data_offset":
				method.setScopesDataOffset(Long.valueOf(entry.getValue()));
				break;
			case "scopes_pcs_offset":
				method.setScopesPcsOffset(Long.valueOf(entry.getValue()));
				break;
			case "dependencies_offset":
				method.setDependenciesOffset(Long.valueOf(entry.getValue()));
				break;
			case "oops_offset":
				method.setOopsOffset(Long.valueOf(entry.getValue()));
				break;
			case "nul_chk_table_offset":
				method.setNulChkTableOffset(Long.valueOf(entry.getValue()));
				break;
			case "consts_offset":
				method.setCountsOffset(Long.valueOf(entry.getValue()));
				break;
			case "method":
				method.setMethod(entry.getValue());
				break;
			case "bytes":
				method.setBytes(Long.valueOf(entry.getValue()));
				break;
			case "count":
				method.setCount(Long.valueOf(entry.getValue()));
				break;
			case "iicount":
				method.setIicount(Long.valueOf(entry.getValue()));
				break;
			case STAMP:
				method.setStamp(Float.valueOf(entry.getValue()));
				break;
			}

		}

	}

	private static void updateTask(Map<String, String> map, Task task) {
		for (Map.Entry<String, String> entry : map.entrySet()) {
			switch (entry.getKey()) {
			case "compile_id":
				task.setCompileId(entry.getValue());
				break;
			case "compile_kind":
				task.setCompileKind(entry.getValue());
				break;
			case METHOD:
				task.setMethod(entry.getValue());
				break;
			case "bytes":
				task.setBytes(Long.valueOf(entry.getValue()));
				break;
			case "count":
				task.setCount(Long.valueOf(entry.getValue()));
				break;
			case "iicount":
				task.setIicount(Long.valueOf(entry.getValue()));
				break;
			case "osr_bci":
				task.setOsrBci(Long.valueOf(entry.getValue()));
				break;
			case "level":
				task.setLevel(Long.valueOf(entry.getValue()));
				break;
			case "blocking":
				task.setBlocking(Boolean.getBoolean(entry.getValue()));
				break;
			case STAMP:
				task.setStamp(Float.valueOf(entry.getValue()));
				break;
			}
		}

	}

	private static void updateTaskDone(Map<String, String> map, Task task) {
		for (Map.Entry<String, String> entry : map.entrySet()) {
			switch (entry.getKey()) {
			case "success":
				task.setSuccess(Long.valueOf(entry.getValue()) == 1 ? true
						: false);
				break;
			case "inlined_bytes":
				task.setInlinedBytes(Long.valueOf(entry.getValue()));
				break;
			case "nmsize":
				task.setNativeMethodSize(Long.valueOf(entry.getValue()));
				break;
			case "backedge_count":
				task.setBackedgeCount(Long.valueOf(entry.getValue()));
				break;
			case STAMP:
				task.setDoneStamp(Float.valueOf(entry.getValue()));
				break;
			}
		}

	}

	private static void updatePhase(Map<String, String> map, Phase phase) {
		for (Map.Entry<String, String> entry : map.entrySet()) {
			switch (entry.getKey()) {
			case "name":
				phase.setName(entry.getValue());
				break;
			case STAMP:
				phase.setStartStamp(Float.valueOf(entry.getValue()));
				break;
			}
		}
	}

	private static void updatePhaseDone(Map<String, String> map, Phase phase) {
		for (Map.Entry<String, String> entry : map.entrySet()) {
			switch (entry.getKey()) {
			case STAMP:
				phase.setEndStamp(Float.valueOf(entry.getValue()));
				break;
			}
		}
	}

	private static Map<String, String> getAttributeValueMap(
			StartElement startElement) {
		Iterator<Attribute> attributeItr = startElement.getAttributes();
		Map<String, String> map = new HashMap<>();
		while (attributeItr.hasNext()) {
			Attribute attr = attributeItr.next();
			map.put(attr.getName().toString(), attr.getValue());

		}
		return map;

	}

	private static void getAttributeValues(StartElement startElement) {
		Iterator<Attribute> attributeItr = startElement.getAttributes();
		while (attributeItr.hasNext()) {
			Attribute attr = attributeItr.next();
			// System.out.print(" " + attr.getName() + ":" + attr.getValue());
		}
	}
}
