package com.threaddoctor.web.thread.service.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.threaddoctor.web.thread.bean.LockedThreadsData;
import com.threaddoctor.web.thread.bean.StacktraceThreadGroup;
import com.threaddoctor.web.thread.bean.ThreadData;
import com.threaddoctor.web.thread.bean.ThreadOverview;
import com.threaddoctor.web.thread.controller.ThreadDataController;
import com.threaddoctor.web.thread.util.DumpParserUtil;

@Service
public class ThreadDataServiceImpl implements ThreadDataService {
	private static final String FINALIZER = "Finalizer";
	private static final String WAITING_TO_LOCK = "Waiting to lock";
	private static final String WAITING_FOR_LOCK = "Waiting for lock";
	private static final String NON_DAEMON = "Non Daemon";
	private static final String GC = "GC";
	private static final String DAEMON = "Daemon";
	private static final String BLOCKED = "BLOCKED";
	private static final String TIMED_WAITING = "TIMED_WAITING";
	private static final String RUNNABLE = "RUNNABLE";
	private static final String WAITING = "WAITING";
	private static final String WAITING_ON_CONDITION = "WAITING ON CONDITION";

	Logger loger = LoggerFactory.getLogger(ThreadDataController.class);

	@Override
	public ThreadOverview getThreadOverview(MultipartFile file) {
		ThreadOverview threadOverview = DumpParserUtil.parseThreadDump(file);
		return manageTheadOverview(threadOverview);
	}

	@Override
	public ThreadOverview getThreadOverviewFromNetworkFile(String fileURL) {
		ThreadOverview threadOverview = DumpParserUtil
				.parseThreadDumpWithFileURL(fileURL);
		if (threadOverview.isError())
			return threadOverview;

		return manageTheadOverview(threadOverview);
	}

	private ThreadOverview manageTheadOverview(ThreadOverview threadOverview) {
		List<ThreadData> threads = threadOverview.getThreads();
		Map<String, List<ThreadData>> threadGroupMap = threadOverview
				.getThreadGroups();
		Map<String, LockedThreadsData> lockHoldingThreads = new HashMap<>();
		Map<String, List<ThreadData>> methodGroup = new HashMap<>();
		Map<Integer, List<ThreadData>> stackTraceMap = new HashMap<>();
		List<StacktraceThreadGroup> stackTraceThreadGroup = new ArrayList<>();

		threadOverview.setLockedThreadDataMap(lockHoldingThreads);
		threadOverview.setMethodGroup(methodGroup);
		threadOverview.setStackTraceMap(stackTraceMap);
		threadOverview.setStackTraceThreadGroup(stackTraceThreadGroup);
		initalizeThreadGroupMap(threadGroupMap);
		int counter = 0;
		for (ThreadData thread : threads) {
			counter++;
			setThreadsByStatus(threadOverview, thread);

			String threadGroup = thread.getGroup();
			List<ThreadData> threadGroupList = threadGroupMap.get(threadGroup);

			if (null != threadGroupList) {
				threadGroupList.add(thread);
			} else {
				threadGroupList = new ArrayList<>();
				threadGroupList.add(thread);
				threadGroupMap.put(threadGroup, threadGroupList);
			}

			setGCStatus(threadGroupMap, thread);
			setDaemonStatus(threadGroupMap, thread);
			setLockHoldingThreads(lockHoldingThreads, thread);
			setWaitingForLockThreads(lockHoldingThreads, thread);

			if (FINALIZER.equalsIgnoreCase(thread.getName())) {
				threadOverview.setFinalizerThread(thread);
			}
			setMethodGroup(thread, methodGroup);
			setThreadStackTrace(thread, stackTraceMap);
			threadOverview.setLockedThreadDataMap(lockHoldingThreads);

		}
		analyzeThreadStackTrace(stackTraceMap, stackTraceThreadGroup);
		loger.debug("Total threads: {}", counter);
		return threadOverview;
	}

	private void setLockHoldingThreads(
			Map<String, LockedThreadsData> lockHoldingThreads, ThreadData thread) {
		if (thread.isHoldLock()) {
			for (String lockId : thread.getLockedIds()) {
				LockedThreadsData lockThreadsData = lockHoldingThreads
						.get(lockId);
				if (lockThreadsData == null) {
					lockThreadsData = new LockedThreadsData();
					lockThreadsData.setLockHoldingThread(thread);
					lockThreadsData.setLockId(lockId);
					lockHoldingThreads.put(lockId, lockThreadsData);
				} else {
					lockThreadsData.setLockHoldingThread(thread);
				}
			}
		}
	}

	private void setWaitingForLockThreads(
			Map<String, LockedThreadsData> lockHoldingThreads, ThreadData thread) {
		if (thread.isWaitingForLock()) {
			for (String lockId : thread.getWaitingForLockIds()) {
				LockedThreadsData lockThreadsData = lockHoldingThreads
						.get(lockId);
				if (lockThreadsData == null) {
					lockThreadsData = new LockedThreadsData();
					lockThreadsData.setLockId(lockId);
					lockThreadsData.getWaitingToHoldThreads().add(thread);
					lockHoldingThreads.put(lockId, lockThreadsData);
				} else {
					lockThreadsData.getWaitingToHoldThreads().add(thread);
				}
			}
		}
	}

	private void setMethodGroup(ThreadData thread,
			Map<String, List<ThreadData>> methodGroup) {
		String method = thread.getMethod();
		List<ThreadData> list = methodGroup.get(method);
		if (list == null) {
			list = new ArrayList<>();
			methodGroup.put(method, list);
		}
		list.add(thread);
	}

	private void setDaemonStatus(Map<String, List<ThreadData>> threadGroupMap,
			ThreadData thread) {
		if (thread.isDaemon()) {
			threadGroupMap.get(DAEMON).add(thread);
		} else {
			threadGroupMap.get(NON_DAEMON).add(thread);
		}
	}

	private void setGCStatus(Map<String, List<ThreadData>> threadGroupMap,
			ThreadData thread) {
		if (thread.isGc()) {
			threadGroupMap.get(GC).add(thread);
		}
	}

	private void setThreadsByStatus(ThreadOverview threadOverview,
			ThreadData thread) {
		if (WAITING.equalsIgnoreCase(thread.getStatus())
				|| WAITING_ON_CONDITION.equals(thread.getStatus())) {
			threadOverview.getWaitingThreads().add(thread);
		} else if (RUNNABLE.equalsIgnoreCase(thread.getStatus())) {
			threadOverview.getRunnableThreads().add(thread);
		} else if (TIMED_WAITING.equalsIgnoreCase(thread.getStatus())) {
			threadOverview.getTimedWaitingThreads().add(thread);
		} else if (BLOCKED.equalsIgnoreCase(thread.getStatus())) {
			threadOverview.getBlockedThreads().add(thread);
		}
	}

	private void setThreadStackTrace(ThreadData thread,
			Map<Integer, List<ThreadData>> stackTraceMap) {
		if (null != thread.getStackTrace()) {
			int size = thread.getStackTrace().size();
			List<ThreadData> threads = stackTraceMap.get(size);
			if (threads == null) {
				threads = new ArrayList<>();
				threads.add(thread);
				stackTraceMap.put(size, threads);
			} else {
				threads.add(thread);
			}
		}
	}

	private List<StacktraceThreadGroup> analyzeThreadStackTrace(
			Map<Integer, List<ThreadData>> stackTraceMap,
			List<StacktraceThreadGroup> threadGroup) {

		for (Integer key : stackTraceMap.keySet()) {
			for (ThreadData thread : stackTraceMap.get(key)) {
				compareEachOther(thread, threadGroup);
			}
		}
		return threadGroup;
	}

	private void compareEachOther(ThreadData thread,
			List<StacktraceThreadGroup> threadGroups) {
		List<String> stackTrace = thread.getStackTrace();
		boolean matched = false;
		for (StacktraceThreadGroup otherThread : threadGroups) {
			List<String> otherStackTrace = otherThread.getStackTrace();
			if (stackTrace.size() != otherStackTrace.size()) {
				continue;
			}
			int i = stackTrace.size() - 1;
			while (i >= 0 && stackTrace.get(i).equals(otherStackTrace.get(i))) {
				i--;

			}
			if (i == -1) {
				matched = true;
				List<ThreadData> threads = otherThread.getThreads();
				if (threads == null) {
					threads = new ArrayList<>();
					otherThread.setThreads(threads);
				}
				threads.add(thread);
			}
		}
		if (!matched) {
			StacktraceThreadGroup threadGroup = new StacktraceThreadGroup();
			threadGroup.setStackTrace(stackTrace);
			List<ThreadData> threads = new ArrayList<>();
			threads.add(thread);
			threadGroup.setThreads(threads);
			threadGroups.add(threadGroup);
			analyzeFirstTimeStackTrace(threadGroup);
		}

	}

	private void analyzeFirstTimeStackTrace(StacktraceThreadGroup threadGroup) {
		List<String> stackTrace = threadGroup.getStackTrace();
		boolean isException = false;
		boolean isSocketRead = false;
		threadGroup.setConsumingCPU(true);
		for (String string : stackTrace) {
			if (!isSocketRead
					&& string
							.contains("at java.net.PlainSocketImpl.socketAccept")) {
				isSocketRead = true;
				threadGroup.setConsumingCPU(false);
			}
			if (!isException
					&& string
							.contains("at java.lang.Throwable.fillInStackTrace")) {
				isException = true;
				threadGroup.setException(true);
			}
		}

	}

	private void initalizeThreadGroupMap(
			Map<String, List<ThreadData>> threadGroupMap) {
		// Put Daemon,non Daemon, GC
		threadGroupMap.put(DAEMON, new ArrayList<>());
		threadGroupMap.put(NON_DAEMON, new ArrayList<>());
		threadGroupMap.put(GC, new ArrayList<>());
		threadGroupMap.put(WAITING_TO_LOCK, new ArrayList<>());
		threadGroupMap.put(WAITING_FOR_LOCK, new ArrayList<>());
	}

}