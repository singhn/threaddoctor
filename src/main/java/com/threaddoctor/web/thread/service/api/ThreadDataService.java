package com.threaddoctor.web.thread.service.api;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.threaddoctor.web.thread.bean.ThreadOverview;

@Service
public interface ThreadDataService {
	ThreadOverview getThreadOverview(MultipartFile file);
	ThreadOverview getThreadOverviewFromNetworkFile(String fileURL);
}
