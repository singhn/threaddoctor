package com.threaddoctor.web.thread.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.web.multipart.MultipartFile;

import com.threaddoctor.web.thread.bean.DeadlockedThreadsData;
import com.threaddoctor.web.thread.bean.ThreadData;
import com.threaddoctor.web.thread.bean.ThreadOverview;

public class DumpParserUtil {

	private static final boolean PRINT_DETAIL = false;

	private DumpParserUtil() {
	}

	static final String VM_INFO_PATTERN = "\\s*Full thread dump (.*):";
	static final String STATE_PATTERN = "\\s*java.lang.Thread.State: (\\w*)\\s*";
	static final String THREAD_DETAIL_PATTERN = "\\s*\"(.*)\"(.*)(daemon)?(.*)nid=(\\w*)\\s*(.*)(\\[(.*)\\])?";
	static final String WAIT_TO_LOCK_PATTERN = "(.*)- waiting to lock <(.*)>(.*)";
	static final String LOCKED_OBJECT_PATTERN = "(.*)- locked <(.*)>(.*)";
	static final String METHOD_PATTERN = "(.*)\\s*at\\s(.*)(\\((.*)\\))";
	static final String DEADLOCK_THREAD_ID_PATTERN = "(.*)\"(.*)\":";
	static final String THREAD_ID_PATTERN = "\\s*prio=(\\d*)\\s*tid=(.*)\\s*nid=(.*)\\s*(runnable|waiting|waiting\\s*on\\s*condition)(.*)";
	static final String JNI_REFERENCES_PATTERN = "(.*)JNI global references:\\s*(\\d*)";

	static Pattern vminfopattern = Pattern.compile(VM_INFO_PATTERN,
			Pattern.CASE_INSENSITIVE);
	static Pattern statepattern = Pattern.compile(STATE_PATTERN,
			Pattern.CASE_INSENSITIVE);
	static Pattern threadDetailPattern = Pattern.compile(THREAD_DETAIL_PATTERN,
			Pattern.CASE_INSENSITIVE);
	static Pattern waitingToLockPattern = Pattern.compile(WAIT_TO_LOCK_PATTERN,
			Pattern.CASE_INSENSITIVE);
	static Pattern lockedObjectPattern = Pattern.compile(LOCKED_OBJECT_PATTERN,
			Pattern.CASE_INSENSITIVE);
	static Pattern methodPattern = Pattern.compile(METHOD_PATTERN,
			Pattern.CASE_INSENSITIVE);
	static Pattern threadIdPattern = Pattern.compile(THREAD_ID_PATTERN,
			Pattern.CASE_INSENSITIVE);
	static Pattern jniReferencesPattern = Pattern.compile(
			JNI_REFERENCES_PATTERN, Pattern.CASE_INSENSITIVE);
	static Pattern deadlockThreadIdPattern = Pattern.compile(
			DEADLOCK_THREAD_ID_PATTERN, Pattern.CASE_INSENSITIVE);

	public static ThreadOverview parseThreadDump(MultipartFile file) {
		try {
			return parseThreadDump(file.getInputStream(),
					file.getOriginalFilename());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ThreadOverview parseThreadDumpWithFileURL(String fileURL) {
		ThreadOverview overview = null;
		try {
			BufferedInputStream in = new BufferedInputStream(
					new URL(fileURL).openStream());
			overview = parseThreadDump(in, fileURL);
		} catch (IOException e) {
			overview = new ThreadOverview();
			overview.setError(true);
			overview.setErrorCode(e.getMessage());
		}
		return overview;
	}

	public static ThreadOverview parseThreadDump(InputStream is, String fileName) {

		List<ThreadData> threadList = new ArrayList<>();
		String vmInfo = "";
		ThreadOverview overview = new ThreadOverview();
		overview.setFileName(fileName);
		try (InputStream fis = is) {
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);

			String line;
			StringBuilder builder = new StringBuilder();
			List<String> stackTrace = new ArrayList<>();
			ThreadData threadData = new ThreadData();
			int waiting = 0;
			int runnable = 0;
			int timedWaiting = 0;
			int blocked = 0;
			int daemon = 0;
			int total = 0;
			int nondaemon = 0;
			int gctotal = 0;
			int index = 0;
			// Process VM info
			while ((line = br.readLine()) != null) {
				if (isVMDetail(line)) {
					Matcher matcher = vminfopattern.matcher(line);
					if (matcher.find()) {
						vmInfo = matcher.group(1);
						overview.setJvmVersion(vmInfo);
						break;
					}
				} else if (isThreadDetail(line) || isJNIRefDetail(line)
						|| isHeapDetail(line) || isDeadlockDetail(line)) {
					break;
				}

			}
			do {
				if (line == null) {
					continue;
				}
				if (isJNIRefDetail(line) || isHeapDetail(line)
						|| isDeadlockDetail(line)) {
					break;
				}
				if (line.indexOf('\"') < 0) {
					stackTrace.add(line + "\n");
					index++;
					if (setThreadStatus(threadData, line)) {
						continue;
					}
					if (setWaitingToLock(threadData, line)) {
						continue;
					}
					if (index == 2) {
						setMethodDetail(threadData, line);
						index++;
						continue;
					}
					setLockedObjectData(threadData, line);
				} else {
					Matcher threadDetailmatcher = threadDetailPattern
							.matcher(line);
					if (threadDetailmatcher.find()) {
						index = 0;
						threadData = new ThreadData();
						stackTrace = new ArrayList<>();
						threadData.setStackTrace(stackTrace);
						builder.setLength(0);
						threadList.add(threadData);
						overview.getThreads().add(threadData);
						total++;
						setThreadDetail(line, threadData, threadDetailmatcher);
					}
				}
			} while ((line = br.readLine()) != null);

			if (isJNIRefDetail(line)) {
				Matcher matcher = jniReferencesPattern.matcher(line);
				if (matcher.find()) {
					overview.setJniReferenceCount(matcher.group(2));
				}
			}

			while ((line = br.readLine()) != null) {
				if (isDeadlockDetail(line)) {
					break;
				}
			}

			// process deadlock detail
			DeadlockedThreadsData deadlockedTherads = null;
			StringBuilder deadlockDetail = null;
			boolean stackInfo = false;
			do {
				if (line == null) {
					continue;
				}
				if (isDeadlockDetail(line)) {

					if (deadlockedTherads != null) {
						deadlockedTherads.setDetail(deadlockDetail.toString());
					}
					deadlockedTherads = new DeadlockedThreadsData();
					overview.getDeadlocks().add(deadlockedTherads);
					deadlockDetail = new StringBuilder();
					stackInfo = false;
					continue;
				}
				if (line.contains("=============================")) {
					continue;
				}
				if (line.contains("Java stack information for the threads listed above")) {
					stackInfo = true;
				}
				if (!stackInfo) {
					Matcher matcher = deadlockThreadIdPattern.matcher(line);
					if (matcher.find()) {
						deadlockedTherads.getThreads().add(matcher.group(2));
					}
				} else {
					deadlockDetail.append(line).append("\n");
				}
			} while ((line = br.readLine()) != null);
			if (deadlockedTherads != null) {
				deadlockedTherads.setDetail(deadlockDetail.toString());
			}
			br.close();

			for (ThreadData thread : threadList) {
				if (PRINT_DETAIL) {
					System.out.println(" *********************** ");
					System.out.println("Name:" + thread.getName());
					System.out.println("Group:" + thread.getGroup());
					System.out.println("Status:" + thread.getStatus());
					System.out.println("Daemon:" + thread.isDaemon());
					System.out.println("GC:" + thread.isGc());
					System.out.println("Waiting to lock:"
							+ thread.isWaitingForLock());
					System.out.println("Have locked object:"
							+ thread.isHoldLock());
					System.out.println("Stacktrace:" + thread.getStackTrace());
				}
			}

			if (PRINT_DETAIL) {
				System.out.println(" ****** Report ******* ");
				System.out.println("VM Information:" + vmInfo);
				System.out.println("Total threads:" + total);
				System.out.println("Waiting threads:" + waiting);
				System.out.println("Timedwaiting threads:" + timedWaiting);
				System.out.println("Runnable threads:" + runnable);
				System.out.println("Blocked threads" + blocked);
				System.out.println("Daemon threads:" + daemon);
				System.out.println("Non daemon threads:" + nondaemon);
				System.out.println("GC threads:" + gctotal);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		overview.setJvmVersion(vmInfo);

		return overview;
	}

	private static void setThreadDetail(String line, ThreadData threadData,
			Matcher threadDetailmatcher) {
		String name = threadDetailmatcher.group(1);
		threadData.setName(name);
		setGroupName(threadData);
		setId(threadData, line);
		threadData.setPrio(threadDetailmatcher.group(3));

		// set Status
		String status = threadDetailmatcher.group(6);
		if (threadData.getStatus() == null) {
			threadData.setStatus(status.toUpperCase().trim());
		}
		// set Daemon
		String group2 = threadDetailmatcher.group(2);
		if (group2 != null && group2.contains("daemon")) {
			threadData.setDaemon(true);
		}
		// set GC
		if (name != null && name.contains("GC")) {
			threadData.setGc(true);
		}
	}

	private static void setId(ThreadData threadData, String line) {
		Matcher threadIdMatcher = threadIdPattern.matcher(line);
		if (threadIdMatcher.find()) {
			threadData.setPrio(threadIdMatcher.group(1));
			threadData.setId(threadIdMatcher.group(2));
			threadData.setNid(threadIdMatcher.group(3));
			threadData.setStatus(threadIdMatcher.group(4).toUpperCase());
		}

	}

	private static void setMethodDetail(ThreadData threadData, String line) {
		Matcher matcher = methodPattern.matcher(line);
		if (matcher.find()) {
			threadData.setMethod(matcher.group(2));
		}
	}

	private static boolean setLockedObjectData(ThreadData threadData,
			String line) {
		Matcher lockedObjectMatcher = lockedObjectPattern.matcher(line);
		if (lockedObjectMatcher.find()) {
			threadData.setHoldLock(true);
			threadData.getLockedIds().add(lockedObjectMatcher.group(2));
			return true;
		}
		return false;
	}

	private static boolean setWaitingToLock(ThreadData threadData, String line) {
		Matcher waitingToLockMatcher = waitingToLockPattern.matcher(line);
		if (waitingToLockMatcher.find()) {
			threadData.setWaitingForLock(true);
			threadData.setLockId(waitingToLockMatcher.group(1));
			threadData.getWaitingForLockId().add(waitingToLockMatcher.group(2));
			return true;
		}
		return false;
	}

	private static boolean setThreadStatus(ThreadData threadData, String line) {
		Matcher stateMatcher = statepattern.matcher(line);
		if (stateMatcher.find()) {
			String status = stateMatcher.group(1);
			threadData.setStatus(status);
			return true;
		}
		return false;
	}

	private static void setGroupName(final ThreadData threadData) {
		String name = threadData.getName();

		if (name != null) {
			int hypenIndex = name.indexOf('-');
			if (hypenIndex > 0) {
				threadData.setGroup(name.substring(0, hypenIndex));
				return;
			}
			int hashIndex = name.indexOf('#');
			if (hashIndex > 0) {
				threadData.setGroup(name.substring(0, hashIndex));
				return;
			}
			int bracesIndex = name.indexOf('(');
			if (hashIndex > 0) {
				threadData.setGroup(name.substring(0, bracesIndex));
				return;
			}
		}
		threadData.setGroup(name);
	}

	private static boolean isVMDetail(String line) {
		return line.contains("Full thread dump");
	}

	private static boolean isDeadlockDetail(String line) {
		return line.contains("Found one Java-level deadlock:");
	}

	private static boolean isHeapDetail(String line) {
		return line.contains("Heap");
	}

	private static boolean isJNIRefDetail(String line) {
		Matcher matcher = jniReferencesPattern.matcher(line);
		return matcher.find();
	}

	private static boolean isThreadDetail(String line) {
		Matcher threadDetailmatcher = threadDetailPattern.matcher(line);
		return threadDetailmatcher.find();
	}

}
