package com.threaddoctor.web.thread.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.threaddoctor.web.thread.bean.ThreadData;
import com.threaddoctor.web.thread.bean.ThreadOverview;
import com.threaddoctor.web.thread.service.api.ThreadDataService;

@Controller
public class StackTracedDataController {
	private static final String THREADS = "threads";
	private static final String REDIRECT_THREADREPORT = "redirect:/threadreport";
	private static final String OVERVIEW = "overview";
	private static final String REDIRECT_COMPARATIVE_THREADREPORT = "redirect:/comparativethreadreport";
	private static final String OVERVIEW_LIST = "overviewlist";
	@Autowired
	ThreadDataService service;

	Logger loger = LoggerFactory.getLogger(StackTracedDataController.class);

	@GetMapping("/stacktraces")
	public String readTDFromNetwork(
			@RequestParam(name = "dumpid", required = false) String dumpId,
			@RequestParam("type") String type, HttpSession session, Model model) {
		if (dumpId != null && !"".equals("dumpId")) {
			ThreadOverview overview = (ThreadOverview) session
					.getAttribute(OVERVIEW + dumpId);
			model.addAttribute(OVERVIEW, overview);
			if (overview == null) {
				loger.debug("Threadreport data not present in session, redirecting to uploadform file.");
				return REDIRECT_THREADREPORT;
			}
		} else {
			ThreadOverview overview = (ThreadOverview) session
					.getAttribute(OVERVIEW);
			if (overview != null) {
				loger.debug("Threadoverview Summary: {}", overview);
				model.addAttribute(OVERVIEW, session.getAttribute(OVERVIEW));
				if ("BLOCKED".equals(type)) {
					List<ThreadData> threads = overview.getBlockedThreads();
					model.addAttribute(THREADS, threads);
				} else if ("RUNNABLE".equals(type)) {
					List<ThreadData> threads = overview.getRunnableThreads();
					model.addAttribute(THREADS, threads);
				} else if ("TIMED_WAITING".equals(type)) {
					List<ThreadData> threads = overview
							.getTimedWaitingThreads();
					model.addAttribute(THREADS, threads);
				} else if ("WAITING".equals(type)) {
					List<ThreadData> threads = overview.getWaitingThreads();
					model.addAttribute(THREADS, threads);
				}
			} else {
				loger.debug("Threadreport data not present in session, redirecting to uploadform file.");
				return REDIRECT_THREADREPORT;
			}
		}
		return "stacktracedata";
	}

}
