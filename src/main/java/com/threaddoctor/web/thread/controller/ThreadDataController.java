package com.threaddoctor.web.thread.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.threaddoctor.web.thread.bean.ThreadOverview;
import com.threaddoctor.web.thread.service.api.ThreadDataService;

@Controller
public class ThreadDataController {
	private static final String REDIRECT_THREADREPORT = "redirect:/threadreport";
	private static final String OVERVIEW = "overview";
	private static final String REDIRECT_COMPARATIVE_THREADREPORT = "redirect:/comparativethreadreport";
	private static final String OVERVIEW_LIST = "overviewlist";
	private static final String REDIRECT_TO_NEW_LOG = "redirect:/uploadform";

	@Autowired
	ThreadDataService service;

	Logger loger = LoggerFactory.getLogger(ThreadDataController.class);

	@GetMapping("/uploadform")
	public String logUploadForm(Model model) {
		return "loguploadform";
	}

	@GetMapping("/networkfile")
	public String readTDFromNetwork(@RequestParam("fileurl") String fileURL,
			HttpSession session, Model model) {
		ThreadOverview overview = service
				.getThreadOverviewFromNetworkFile(fileURL);
		loger.debug("File Location: {}", fileURL);
		session.setAttribute(OVERVIEW, overview);
		return REDIRECT_THREADREPORT;
	}

	@PostMapping("/upload")
	public String uploadFile(MultipartFile file, HttpSession session,
			Model model) {
		ThreadOverview overview = service.getThreadOverview(file);
		loger.debug("File name: {}", file.getName());
		session.setAttribute(OVERVIEW, overview);
		return REDIRECT_THREADREPORT;
	}

	@GetMapping("/threadreport")
	public String getThreadReport(
			@RequestParam(name = "dumpid", required = false) String dumpId,
			Model model, HttpSession session) {

		if (dumpId != null && !"".equals("dumpId")) {
			ThreadOverview overview = (ThreadOverview) session
					.getAttribute(OVERVIEW + dumpId);
			model.addAttribute(OVERVIEW, overview);
			if (overview == null) {
				loger.debug("Threadreport data not present in session, redirecting to uploadform file.");
				return REDIRECT_TO_NEW_LOG;
			}
		} else {
			ThreadOverview overview = (ThreadOverview) session
					.getAttribute(OVERVIEW);
			if (overview != null) {
				loger.debug("Threadoverview Summary: {}", overview);
				model.addAttribute(OVERVIEW, session.getAttribute(OVERVIEW));
			} else {
				loger.debug("Threadreport data not present in session, redirecting to uploadform file.");
				return REDIRECT_TO_NEW_LOG;
			}
		}
		return "threaddata";
	}

	@GetMapping("/comparativethreadreport")
	public String getComparativeThreadReport(Model model, HttpSession session) {
		List<ThreadOverview> overviewList = (List<ThreadOverview>) session
				.getAttribute(OVERVIEW_LIST);
		if (overviewList != null) {
			loger.debug("Threadoverview Summary: {}", overviewList);
			model.addAttribute(OVERVIEW_LIST,
					session.getAttribute(OVERVIEW_LIST));
			int i = 0;
			for (ThreadOverview overview : overviewList) {
				session.setAttribute(OVERVIEW + i, overview);
				i++;
			}
		} else {
			loger.debug("Threadreport data not present in session, redirecting to uploadform file.");
			return REDIRECT_COMPARATIVE_THREADREPORT;
		}
		return "comparativethreaddata";
	}

	@GetMapping("/networkfiles")
	public String readMultipleTDFromNetwork(
			@RequestParam("fileurl") String fileURL, HttpSession session,
			Model model) {
		System.out.println(fileURL);
		String[] files = fileURL.split(";");
		System.out.println(files[0]);
		List<ThreadOverview> overviewList = new ArrayList<>(files.length);
		for (String file : files) {
			loger.debug("File Location: {}", file);
			System.out.println(file);
			overviewList.add(service.getThreadOverviewFromNetworkFile(file));
		}

		session.setAttribute(OVERVIEW_LIST, overviewList);
		return REDIRECT_COMPARATIVE_THREADREPORT;
	}
}
