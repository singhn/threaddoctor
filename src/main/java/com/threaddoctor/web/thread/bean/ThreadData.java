package com.threaddoctor.web.thread.bean;

import java.util.ArrayList;
import java.util.List;

public class ThreadData {
	private String id;
	private String name;
	private String prio;
	private String nid;
	private String status;
	private String group;
	private List<String> waitingForLockIds = new ArrayList<>();
	private List<String> lockedIds = new ArrayList<>();
	private boolean daemon;
	private boolean gc;
	private List<String> stackTrace;
	private boolean waitingForLock;
	private boolean holdLock;
	private String lockId;
	private String method;
	private String className;
	private boolean consumingCPU;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public boolean isDaemon() {
		return daemon;
	}

	public void setDaemon(boolean daemon) {
		this.daemon = daemon;
	}

	public boolean isGc() {
		return gc;
	}

	public void setGc(boolean gc) {
		this.gc = gc;
	}

	public String getPrio() {
		return prio;
	}

	public void setPrio(String prio) {
		this.prio = prio;
	}

	public String getNid() {
		return nid;
	}

	public void setNid(String nid) {
		this.nid = nid;
	}

	public void setWaitingForLock(boolean waitingForLock) {
		this.waitingForLock = waitingForLock;

	}

	public boolean isWaitingForLock() {
		return waitingForLock;
	}

	public List<String> getWaitingForLockId() {
		return waitingForLockIds;
	}

	public void setWaitingForLockIds(List<String> waitingForLockId) {
		this.waitingForLockIds = waitingForLockId;
	}

	public List<String> getLockedIds() {
		return lockedIds;
	}

	public void setLockedIds(List<String> lockedIds) {
		this.lockedIds = lockedIds;
	}

	public List<String> getWaitingForLockIds() {
		return waitingForLockIds;
	}

	public void setStackTrace(List<String> stackTrace) {
		this.stackTrace = stackTrace;
	}

	public List<String> getStackTrace() {
		return stackTrace;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getLockId() {
		return lockId;
	}

	public void setLockId(String lockId) {
		this.lockId = lockId;
	}

	public boolean isHoldLock() {
		return holdLock;
	}

	public void setHoldLock(boolean holdLock) {
		this.holdLock = holdLock;
	}

	@Override
	public String toString() {
		return "ThreadData [id=" + id + ", name=" + name + ", prio=" + prio
				+ ", nid=" + nid + ", status=" + status + ", group=" + group
				+ ", waitingForLockIds=" + waitingForLockIds + ", lockedIds="
				+ lockedIds + ", daemon=" + daemon + ", gc=" + gc
				+ ", stackTrace=" + stackTrace + ", waitingForLock="
				+ waitingForLock + ", holdLock=" + holdLock + ", lockId="
				+ lockId + ", method=" + method + ", className=" + className
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ThreadData other = (ThreadData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public boolean isConsumingCPU() {
		return consumingCPU;
	}

	public void setConsumingCPU(boolean consumingCPU) {
		this.consumingCPU = consumingCPU;
	}
}
