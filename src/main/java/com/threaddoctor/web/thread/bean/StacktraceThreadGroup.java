package com.threaddoctor.web.thread.bean;

import java.util.List;

public class StacktraceThreadGroup {
	private List<String> stackTrace;
	private List<ThreadData> threads;
	private boolean consumingCPU;
	private boolean isException;

	public List<String> getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(List<String> stackTrace) {
		this.stackTrace = stackTrace;
	}

	public List<ThreadData> getThreads() {
		return threads;
	}

	public void setThreads(List<ThreadData> threads) {
		this.threads = threads;
	}

	public boolean isConsumingCPU() {
		return consumingCPU;
	}

	public void setConsumingCPU(boolean consumingCPU) {
		this.consumingCPU = consumingCPU;
	}

	public boolean isException() {
		return isException;
	}

	public void setException(boolean isException) {
		this.isException = isException;
	}
}
