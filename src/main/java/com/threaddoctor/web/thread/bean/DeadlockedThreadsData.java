package com.threaddoctor.web.thread.bean;

import java.util.ArrayList;
import java.util.List;

public class DeadlockedThreadsData {
	private List<String> threads;
	private String detail;

	public List<String> getThreads() {
		if (threads == null) {
			threads = new ArrayList<>();
		}
		return threads;
	}

	public void setThreads(List<String> threads) {
		this.threads = threads;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		return "DeadlockedTherads [threads=" + threads + ", detail=" + detail
				+ "]";
	}

}
