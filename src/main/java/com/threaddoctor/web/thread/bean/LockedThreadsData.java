package com.threaddoctor.web.thread.bean;

import java.util.ArrayList;
import java.util.List;

public class LockedThreadsData {
	private String lockId;
	private ThreadData lockHoldingThread;
	private List<ThreadData> waitingToHoldThreads;

	public String getLockId() {
		return lockId;
	}

	public void setLockId(String lockId) {
		this.lockId = lockId;
	}

	public ThreadData getLockHoldingThread() {
		return lockHoldingThread;
	}

	public void setLockHoldingThread(ThreadData lockHoldingThread) {
		this.lockHoldingThread = lockHoldingThread;
	}

	public List<ThreadData> getWaitingToHoldThreads() {
		if (waitingToHoldThreads == null) {
			waitingToHoldThreads = new ArrayList<>();
		}
		return waitingToHoldThreads;
	}

	public void setWaitingToHoldThreads(List<ThreadData> waitingToHoldThreads) {
		this.waitingToHoldThreads = waitingToHoldThreads;
	}

	@Override
	public String toString() {
		return "LockedThreadsData [lockId=" + lockId + ", lockHoldingThread="
				+ lockHoldingThread + ", waitingToHoldThreads="
				+ waitingToHoldThreads + "]";
	}
}
