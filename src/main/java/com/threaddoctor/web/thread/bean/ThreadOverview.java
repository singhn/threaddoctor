package com.threaddoctor.web.thread.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ThreadOverview implements Serializable {
	private static final long serialVersionUID = 1L;
	private String fileName;
	private String jvmVersion;
	private String jniReferenceCount;
	private ThreadData finalizerThread;
	private List<ThreadData> threads;
	private List<ThreadData> waitingThreads;
	private List<ThreadData> runnableThreads;
	private List<ThreadData> blockedThreads;
	private List<ThreadData> timedWaitingThreads;
	private List<DeadlockedThreadsData> deadlocks;
	private Map<String, List<ThreadData>> threadGroups;
	private Map<String, LockedThreadsData> lockedThreadDataMap;
	private Map<String, List<ThreadData>> methodGroup;
	private Map<Integer, List<ThreadData>> stackTraceMap;
	private List<StacktraceThreadGroup> stackTraceThreadGroup;
	private String errorCode;
	private boolean error;

	public int getTotalThreadCount() {
		return getThreads().size();
	}

	public int getWaitingThreadCount() {
		return getWaitingThreads().size();
	}

	public int getRunnableThreadCount() {
		return getRunnableThreads().size();
	}

	public int getBlockedThreadCount() {
		return getBlockedThreads().size();
	}

	public int getTimedWaitingThreadCount() {
		return getTimedWaitingThreads().size();
	}

	public List<ThreadData> getThreads() {
		if (threads == null) {
			threads = new ArrayList<>();
		}
		return threads;
	}

	public void setThreads(List<ThreadData> threads) {
		this.threads = threads;
	}

	public List<ThreadData> getWaitingThreads() {
		if (waitingThreads == null) {
			waitingThreads = new ArrayList<>();
		}
		return waitingThreads;
	}

	public void setWaitingThreads(List<ThreadData> waitingThreads) {
		this.waitingThreads = waitingThreads;
	}

	public List<ThreadData> getRunnableThreads() {
		if (runnableThreads == null) {
			runnableThreads = new ArrayList<>();
		}
		return runnableThreads;
	}

	public void setRunnableThreads(List<ThreadData> runnableThreads) {
		this.runnableThreads = runnableThreads;
	}

	public List<ThreadData> getBlockedThreads() {
		if (blockedThreads == null) {
			blockedThreads = new ArrayList<>();
		}
		return blockedThreads;
	}

	public void setBlockedThreads(List<ThreadData> blockedThreads) {
		this.blockedThreads = blockedThreads;
	}

	public List<ThreadData> getTimedWaitingThreads() {
		if (timedWaitingThreads == null) {
			timedWaitingThreads = new ArrayList<>();
		}
		return timedWaitingThreads;
	}

	public void setTimedWaitingThreads(List<ThreadData> timedWaitingThreads) {
		this.timedWaitingThreads = timedWaitingThreads;
	}

	public Map<String, List<ThreadData>> getThreadGroups() {
		if (threadGroups == null) {
			threadGroups = new LinkedHashMap<>();
		}
		return threadGroups;
	}

	public void setThreadGroups(Map<String, List<ThreadData>> threadGroups) {
		this.threadGroups = threadGroups;
	}

	public String getJvmVersion() {
		return jvmVersion;
	}

	public void setJvmVersion(String jvmVersion) {
		this.jvmVersion = jvmVersion;
	}

	public Map<String, LockedThreadsData> getLockedThreadDataMap() {
		return lockedThreadDataMap;
	}

	public void setLockedThreadDataMap(
			Map<String, LockedThreadsData> lockedThreadDataMap) {
		this.lockedThreadDataMap = lockedThreadDataMap;
	}

	public String getJniReferenceCount() {
		return jniReferenceCount;
	}

	public void setJniReferenceCount(String jniReferenceCount) {
		this.jniReferenceCount = jniReferenceCount;
	}

	public List<DeadlockedThreadsData> getDeadlocks() {
		if (deadlocks == null) {
			deadlocks = new ArrayList<>();
		}
		return deadlocks;
	}

	public void setDeadlocks(List<DeadlockedThreadsData> deadlocks) {
		this.deadlocks = deadlocks;
	}

	public ThreadData getFinalizerThread() {
		return finalizerThread;
	}

	public void setFinalizerThread(ThreadData finalizerThread) {
		this.finalizerThread = finalizerThread;
	}

	public Map<String, List<ThreadData>> getMethodGroup() {
		return methodGroup;
	}

	public void setMethodGroup(Map<String, List<ThreadData>> methodGroup) {
		this.methodGroup = methodGroup;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Map<Integer, List<ThreadData>> getStackTraceMap() {
		return stackTraceMap;
	}

	public void setStackTraceMap(Map<Integer, List<ThreadData>> stackTraceMap) {
		this.stackTraceMap = stackTraceMap;
	}

	public List<StacktraceThreadGroup> getStackTraceThreadGroup() {
		return stackTraceThreadGroup;
	}

	public void setStackTraceThreadGroup(
			List<StacktraceThreadGroup> stackTraceThreadGroup) {
		this.stackTraceThreadGroup = stackTraceThreadGroup;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

}