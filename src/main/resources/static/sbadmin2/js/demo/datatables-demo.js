// Call the dataTables jQuery plugin
$(document).ready(function() {
 $('#threadGroupDataTable').dataTable({
  "order": [[ 1, 'desc' ]]
  });
 $('#methodGroupDataTable').dataTable({
	  "order": [[ 1, 'desc' ]]
	  });
 $('#gcDataTable').dataTable({
	  "order": [[ 1, 'desc' ]]
	  });
 $('#stackTraceDataTable').dataTable({
	  "order": [[ 1, 'desc' ]]
	  });
 $('#exceptionDataTable').dataTable({
	  "order": [[ 1, 'desc' ]]
	  });
 
  /*
   * // Call the dataTables jQuery plugin
$(document).ready(function() {
 /* $('#threadGroupDataTable').DataTable(){
  "order": [[ 1, 'desc' ]]
  };
  $('#dataTable').DataTable();
  $('#methodGroupDataTable').DataTable(){
  "order": [[ 1, 'desc' ]]
  };
  
});

   */
});
