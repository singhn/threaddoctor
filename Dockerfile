FROM gradle:6.5.1-jdk8 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build -x test --no-daemon 

FROM adoptopenjdk/openjdk13:alpine-slim
RUN mkdir /app
COPY --from=build /home/gradle/src/build/libs/threaddoctor-0.0.1-SNAPSHOT.jar /app/app.jar

ARG JDK_SPECIFIC_CONFIG="-Djava.security.egd=file:/dev/./urandom "
ARG DEFAULT_JAVA_OPTS="-Xms256m -Xmx4096m $JDK_SPECIFIC_CONFIG"
ENV JAVA_OPTS=$DEFAULT_JAVA_OPTS


ENTRYPOINT ["java", $JAVA_OPTS, "-jar", "app/app.jar"]