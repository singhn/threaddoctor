# What?
This application parses Java threaddumps and displays report.

# How to build?

gradlew build -x test

# How to run?

java -jar build\libs\<application-name>.jar

# How to access?
Access below url from web browser.
localhost:8084/threaddoctor

# How to use?
By Uploading File
http://localhost:8084/threaddoctor/uploadform
Upload the threaddump file, post analysis a nice report will be displayed.

By Accessing Network File
http://localhost:8084/threaddoctor/networkfile?fileurl=<file-url>


